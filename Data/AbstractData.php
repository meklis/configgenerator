<?php
/**
 * Created by PhpStorm.
 * User: Meklis
 * Date: 09.02.2018
 * Time: 0:23
 */

namespace Meklis\ConfigGenerator\Data;


use Meklis\ConfigGenerator\Exceptions\NotFoundException;
use Meklis\ConfigGenerator\Exceptions\RemoteDeviceException;

abstract class AbstractData implements DataInterface
{
    protected $sql;
    protected $dir;
    protected $ip;
    protected $switch = [
        'swid' => 0,
        'ip' => '',
        'login' => '',
        'pass' => '',
        'community' => '',
        'dir' => '',
        'inet_vlan' => '',
    ];
    protected $uplinkPort;
    protected $description;
    protected $firmware;
    protected $databaseParam;

    function __construct()
    {
        $this->sql = dbConn('cr1');
    }

    function getBindings()
    {
        $binds = [];
        $data = \msdb::getBindBySw($this->ip, true);
        foreach ($data as $num => $bind) {
            $block = "ADD_ACL_PROFILES";
            if ($bind['ip'] == '1.1.1.1' || $bind['ip'] == '3.3.3.3') continue;
            if ($bind['ip'] == '2.2.2.2') {
                $num += 500;
                $block = "ADD_MULT_PROFILES";
            }
            $binds[$num] = [
                'block' => $block,
                'PORT' => $bind['port'],
                'IP' => $bind['ip'],
                'RULE' => 2,
            ];
        }
        ksort($binds);
        return array_values($binds);
    }

    function getDatabaseParam()
    {
        if (!$this->dir) {
            $this->dir = $this->sql->query("SELECT sw.dir
                    FROM mrtg.ORIGIN_DIRECT o 
                    JOIN mrtg.switches sw on sw.ip  = o.router 
                    WHERE INET_ATON('{$this->switch['ip']}') BETWEEN INET_ATON(network) and INET_ATON(broadcast) LIMIT 1")->fetch_assoc()['dir'];
            if (!$this->dir) {
                throw new InvalidArgumentException("Not found network for ip {$this->switch['ip']}");
            };
        }
        if ($this->databaseParam) {
            return $this->databaseParam;
        }
        $dbName = "";
        switch ($this->dir) {
            case 'Оболонь':
                $dbName = "obolon";
                break;
            case 'Салтовка':
                $dbName = "saltovka";
                break;
            case 'Троещина':
                $dbName = "troya";
                break;
            case 'Рогань':
                $dbName = "rogan";
                break;
        }
        $data = $this->sql->query("SELECT `key`, `value` FROM `$dbName`.`ConfigGenerator`");
        $params = [];
        while ($d = $data->fetch_assoc()) {
            $params[$d['key']] = json_decode($d['value'], true);
        }
        $this->databaseParam = $params;
        return $params;
    }

    function getMagistralPorts()
    {
        if ($this->uplinkPort) return $this->uplinkPort;
        $walk = new \GetUplinkPort();
        if (!$walk->open($this->ip, $this->switch['community'], $this->switch['login'], $this->switch['pass'])) {
            throw  new RemoteDeviceException("Switch not respond by snmp");
        };
        if (!$port = $walk->getData()) {
            throw  new RemoteDeviceException("Switch not returned uplink port");
        }
        $this->uplinkPort = $port;
        return $port;
    }

    function getDescription()
    {
        if ($this->description) return $this->description;
        $walk = @snmpwalk($this->ip, $this->switch['community'], "1.3.6.1.2.1.1.1");
        if (!$walk) {
            throw new RemoteDeviceException("Switch not responced with community {$this->switch['community']}");
        }
        $walk = str_replace(["\"", "STRING: "], "", $walk[0]);
        $this->description = $walk;
        return $walk;
    }

    function getDhcpRelay()
    {
        $vlan = $this->getInetVlan();
        $mikro = $this->sql->query("SELECT sw.ip, sw.login, sw.`password`
        FROM mrtg.switches sw 
        JOIN mrtg.ORIGIN_DIRECT d on d.router = sw.ip 
        WHERE d.vlan =  $vlan and model like '%mikrotik%' LIMIT 1")->fetch_assoc();
        if (!$connect = \devStd::newMikro($mikro['ip'], $mikro['login'], $mikro['password'])) {
            throw new RemoteDeviceException("Error connecting to mikrotik {$mikro['ip']}");
        }
        $command = $connect->comm("/ip/dhcp-server/print");
        $interface = "";
        foreach ($command as $com) {
            if (@$com['disabled'] === 'false') {
                $interface = @$com['interface'];
            }
        }
        if (!$interface) {
            throw  new NotFoundException("DHCP-relay server on mikrotik {$mikro['ip']} not found");
        }
        $gateway = $this->sql->query("SELECT gateway FROM mrtg.ORIGIN_DIRECT WHERE vlan_name = '{$interface}' and router = '{$mikro['ip']}' LIMIT 1")->fetch_assoc()['gateway'];
        if (!$gateway) {
            throw  new NotFoundException("Gateway not found in CR1 database. Search by interface: {$interface} and router: {$mikro['ip']}");
        }
        return $gateway;
    }
    function getPingerByIP($switchIP) {
        $data = $this->sql->query("SELECT DISTINCT c.serv_ip  
                FROM mrtg.ORIGIN_DIRECT d 
                JOIN mrtg.switches sw on sw.ip = d.router
                JOIN netstat.PingerServersToCity c on c.city = sw.city 
                WHERE INET_ATON('$switchIP') BETWEEN INET_ATON(d.network) and INET_ATON(d.broadcast)")->fetch_assoc()['serv_ip'];
        return $data;
    }

    function getSwitchParams()
    {
        $data = $this->sql->query("SELECT DISTINCT vlan, gateway,  INET_ATON(broadcast) - INET_ATON(network) pool 
                              FROM mrtg.ORIGIN_DIRECT WHERE INET_ATON('{$this->ip}') BETWEEN INET_ATON(network) and INET_ATON(broadcast) LIMIT 1")->fetch_assoc();
        if (!$data['pool']) throw new NotFoundException("Not found network for ip {$this->ip}");
        return [
            'ip' => $this->ip,
            'vlanId' => $data['vlan'],
            'vlanName' => "switches" . $data['vlan'],
            'gateway' => $data['gateway'],
            'switchesCidr' => Helper::getMask($data['pool']),
            'login' => $this->switch['login'],
            'pass' => $this->switch['pass'],
            'community' => $this->switch['community'],
        ];
    }

    function getFirmware()
    {
        $model = Helper::getDbModel($this->getDescription());
        $oid = $this->sql->query("SELECT oid FROM mrtg.OIDS WHERE type = 'sysFirmware' and model = '$model' LIMIT 1")->fetch_assoc()['oid'];
        $resp = \mSNMP::walk($oid, $this->ip, $this->switch['community']);
        if (!$resp) throw new RemoteDeviceException("Error get firmware for switch {$this->ip}, oid: $oid");
        return array_values($resp)[0];
    }

    abstract function getInetVlan();
}