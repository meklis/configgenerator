<?php

namespace Meklis\ConfigGenerator\Data;


interface DataInterface
{
    /**
     * @return int  VlanID INTERNET
     */
    function getInetVlan();

    /**
     *   'ip' => 'SwitchIP',
     *   'vlanId' => 'SwitchVlanId',
     *   'vlanName' => 'SwitchVlanName',
     *   'gateway' => 'Gateway IP',
     *   'switchesCidr' => 'CIDR mask for switch network',
     *   'login' => 'telnet login for switch',
     *   'pass' => 'telnet password to switch',
     *   'community' => 'snmp community',
     *
     * @return array
     */
    function getSwitchParams();

    /**
     * @return array int[25,26,27]
     */
    function getMagistralPorts();

    /**
     * @return string Switch snmp description
     */
    function getDescription();

    /**
     * @return string IP address of DHCP-relay server
     */
    function getDhcpRelay();

    /**
     * @return array
     */
    function getBindings();

    /**
     * @return string firmware of switch from snmp
     */
    function getFirmware();

    /**
     * @return  array [KEY=>VALUE] params from Database
     */
    function getDatabaseParam();

    /**
     * @param $switch
     * @return mixed
     */
    function getPingerByIP($switch);
}
