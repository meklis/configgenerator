<?php
/**
 * Created by PhpStorm.
 * User: Meklis
 * Date: 08.02.2018
 * Time: 17:30
 */

namespace Meklis\ConfigGenerator\Data;

use Meklis\ConfigGenerator\Exceptions\InvalidArgumentException;

class Helper
{
    static private $modelsAssoc = [
        'P3310B' => 'BDCOM P3310',
        'DES-1210-28/ME/B3' => 'DES-1210-28/ME/B3',
        'DES-1210-28/ME' => 'DES 1210-28ME',
        'DES-1210-28' => 'DES-1210-28',
        'DES-1210-26/ME' => 'DES 1210-26ME',
        'D-Link DES-1228/ME Metro Ethernet Switch' => 'DES-1228/ME',
        'DES-2110' => 'DES-2110',
        'Dlink DES-3010G Fast Ethernet Switch' => 'DES-3010G',
        'Dlink DES-3026 Fast Ethernet Switch' => 'DES-3026',
        'D-Link DES-3028 Fast Ethernet Switch' => 'DES-3028',
        'D-Link DES-3028G Fast Ethernet Switch' => 'DES-3028G',
        'D-Link DES-3052 Fast Ethernet Switch' => 'DES-3052',
        'D-Link DES-3200-10 Fast Ethernet Switch' => 'DES-3200-10',
        'DES-3200-10/C1 Fast Ethernet Switch' => 'DES-3200-10-C1',
        'DES-3200-10 Fast Ethernet' => 'DES-3200-10',
        'D-Link DES-3200-18 Fast Ethernet Switch' => 'DES-3200-18',
        'DES-3200-18/C1 Fast Ethernet Switch' => 'DES-3200-18-C1',
        'D-Link DES-3200-26 Fast Ethernet Switch' => 'DES-3200-26',
        'DES-3200-26/C1 Fast Ethernet Switch' => 'DES-3200-26-C1',
        'DES-3200-28/C1 Fast Ethernet Switch' => 'DES-3200-28-C1',
        'D-Link DES-3200-28F Fast Ethernet Switch' => 'DES-3200-28F',
        'DES-3200-28F Fast Ethernet' => 'DES-3200-28F',
        'DES-3200-28F/C1 Fast Ethernet Switch' => 'DES-3200-28F-c1',
        'DES-3200-52/C1 Fast Ethernet Switch' => 'DES-3200-52',
        'DES-3526 Fast-Ethernet Switch' => 'DES-3526',
        'DES-3550 Fast-Ethernet Switch' => 'DES-3550',
        'DES-3828 Fast-Ethernet Switch' => 'DES-3828',
        'DGS-1100-06/ME' => 'DGS-1100-06/ME',
        'DGS-1100-06/ME/A1' => 'DGS-1100-06/ME',
        'DGS-1100-10/ME' => 'DGS-1100-10/ME',
        'DGS-1210-20 ' => 'DGS-1210-20',
        'DGS-1210-28XS/ME' => 'DGS-1210-28XS/ME',
        'DGS-3000-10TC Gigabit Ethernet Switch' => 'DGS-3000-10TC',
        'DGS-3000-26TC Gigabit Ethernet Switch' => 'DGS-3000-26TC',
        'DGS-3000-28SC Gigabit Ethernet Switch' => 'DGS-3000-28SC',
        'DGS-3100-24 Gigabit stackable L2 Managed Switch' => 'DGS-3100-24',
        'DGS-3100-24TG  Gigabit stackable L2 Managed Switch' => 'DGS-3100-24TG',
        'DGS-3100-48 Gigabit stackable L2 Managed Switch' => 'DGS-3100-48',
        'DGS-3120-24SC Gigabit Ethernet Switch' => 'DGS-3120-24SC',
        'DGS-3120-24TC Gigabit Ethernet Switch' => 'DGS-3120-24-TC',
        'DGS-3200-10 Gigabit Ethernet Switch' => 'DGS-3200-10-A1',
        'DGS-3324SR Stackable Ethernet Switch' => 'DGS-3324SR',
        'DGS-3420-26SC Gigabit Ethernet Switch' => 'DGS-3420-26SC',
        'DGS-3420-28SC Gigabit Ethernet Switch' => 'DGS-3420-28SC',
        'DGS-3420-28TC Gigabit Ethernet Switch' => 'DGS-3420-28TC',
        'RouterOS' => 'Mikrotik',
        'Extreme' => 'Summit',
        'DES-3200-28 Fast Ethernet Switch' => 'DES-3200-28',
        'DGS-1510-28XS/ME Gigabit Ethernet Switch' => 'DGS-1510-28XS/ME',
        'P3310C' => 'BDCOM P3310C',
        'XOS' => 'Summit',
        'DGS-1210-10/ME/A1' => 'DGS-1210-10/ME rev. A',
        'DGS-1210-10/ME/B1' => 'DGS-1210-10/ME rev. B',
        'DGS-1210-20/ME/B1' => 'DGS-1210-20/ME rev. B',
        'DGS-1210-28/ME/A2' => 'DGS-1210-28/ME rev. A',
    ];

    static function groupPortByArray($array)
    {
        sort($array);
        $array = array_values($array);
        $result = "";
        $buffer = false;
        foreach ($array as $num => $port) {
            if (isset($array[$num + 1]) && $buffer && $array[$num + 1] == $port + 1) {
                continue;
            } elseif (isset($array[$num + 1]) && !$buffer && $array[$num + 1] == $port + 1) {
                $buffer = "{$port}-";
            } elseif ((isset($array[$num + 1]) && $array[$num + 1] != $port + 1) || !isset($array[$num + 1])) {
                $result .= $buffer . $port . ",";
                $buffer = false;
            } else {
                $result .= $port . ",";
            }
        }
        return trim($result, ",");
    }

    public static function getMask($pool)
    {
        if ($pool <= 1) return 32;
        if ($pool <= 2) return 31;
        if ($pool <= 4) return 30;
        if ($pool <= 8) return 29;
        if ($pool <= 16) return 28;
        if ($pool <= 32) return 27;
        if ($pool <= 64) return 26;
        if ($pool <= 128) return 25;
        if ($pool <= 256) return 24;
        if ($pool <= 512) return 23;
        if ($pool <= 1024) return 22;
        if ($pool <= 2048) return 21;
        if ($pool <= 4096) return 20;
        if ($pool <= 8192) return 19;
        if ($pool <= 16384) return 18;
        if ($pool <= 32768) return 17;
        if ($pool <= 65536) return 16;
        if ($pool <= 131072) return 15;
        if ($pool <= 262144) return 14;
        if ($pool <= 524288) return 13;
        if ($pool <= 1048576) return 12;
        if ($pool <= 2097152) return 11;
        if ($pool <= 4194304) return 10;
        if ($pool <= 8388608) return 9;
        if ($pool <= 16777216) return 8;
        if ($pool <= 33554432) return 7;
        if ($pool <= 67108864) return 6;
        if ($pool <= 134217728) return 5;
        if ($pool <= 268435456) return 4;
        if ($pool <= 536870912) return 3;
        if ($pool <= 1073741824) return 2;
        if ($pool <= 2147483648) return 1;
        if ($pool <= 4294967296) return 0;
        throw new InvalidArgumentException("Incorrect pool size for get mask");
    }

    static function getDbModel($description)
    {
        foreach (static::$modelsAssoc as $d => $m) {
            if (preg_match("*" . quotemeta($d) . "*", $description)) return $m;
        }
        return $description;
    }
}