<?php
/**
 * Created by PhpStorm.
 * User: Meklis
 * Date: 05.02.2018
 * Time: 22:15
 */

namespace Meklis\ConfigGenerator\Data;

class DataNoInDb extends AbstractData implements DataInterface
{
    protected $sql;
    protected $dir;
    protected $uplinkPort = false;
    protected $switch = [
        'swid' => 0,
        'ip' => '',
        'login' => '',
        'pass' => '',
        'community' => '',
        'dir' => '',
        'inet_vlan' => '',
    ];

    function __construct($host)
    { 
        parent::__construct();
        $this->switch = $host;
        $this->ip = $host['ip'];
    }

    function getInetVlan()
    {
        return $this->switch['inet_vlan'];
    }
}