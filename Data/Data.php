<?php
/**
 * Created by PhpStorm.
 * User: Meklis
 * Date: 05.02.2018
 * Time: 22:15
 */

namespace Meklis\ConfigGenerator\Data;


use Meklis\ConfigGenerator\Exceptions\NotFoundException;
use Meklis\ConfigGenerator\Exceptions\RemoteDeviceException;

class Data extends AbstractData implements DataInterface
{
    protected $sql;
    protected $ip;
    protected $snmp;
    protected $uplinkPort = false;
    protected $switch = [
        'swid' => 0,
        'ip' => '',
        'login' => '',
        'pass' => '',
        'community' => '',
        'house' => 0,
    ];
    protected $description;

    function __construct($host)
    {
        parent::__construct();
        $this->ip = $host;
        $switch = \msdb::getSwAccess($this->ip);
        if (!$switch) {
            throw new NotFoundException("Switch not found in database, please check IP");
        }
        $switch['ip'] = $switch['Ip'];
        unset($switch['Ip']);
        $this->switch = $switch;
    }

    function getInetVlan()
    {
        $vlans = \msdb::getVlanOnSw($this->ip);
        if (!isset($vlans[0]['Vid'])) throw new NotFoundException("Vlans for this switch not found in database");
        return $vlans[0]['Vid'];
    }
}