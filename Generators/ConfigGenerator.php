<?php
/**
 * Created by PhpStorm.
 * User: Meklis
 * Date: 06.02.2018
 * Time: 12:22
 */

namespace Meklis\ConfigGenerator\Generators;


use Meklis\ConfigGenerator\Data\DataInterface;
use Meklis\ConfigGenerator\Data\Helper;
use Meklis\ConfigGenerator\Exceptions\RemoteDeviceException;
use Meklis\ConfigGenerator\Exceptions\InvalidArgumentException;
use Meklis\ConfigGenerator\Loader;

class ConfigGenerator
{
    /**
     * @var DataInterface
     */
    protected $data;

    function __construct(DataInterface $data)
    {
        $this->data = $data;
    }

    function getStartConfig($templateName, $extraParams = [])
    {
        $loader = new Loader\StartConfigLoader($templateName, $this->data->getDescription());
        $parser = new Loader\ConfigParser();
        if ($allowFw = $loader->getFirmwareRevision()) {
            $realFw = $this->data->getFirmware();
            if (!$this->checkFirmware($allowFw, $realFw)) {
                throw new RemoteDeviceException("The firmware of the device does not meet the requirements. Allow: $allowFw, on device: $realFw");
            }
        }
        //Loading params from database
        foreach ($this->data->getDatabaseParam() as $key => $value) {
            $loader->setParam($key, $value);
        }

        //Loading extra params
        foreach ($extraParams as $key => $value) {
            $loader->setParam($key, $value);
        }


        $loader->setParam("ABON_PORTS", Helper::groupPortByArray($this->getAbonPorts($this->data, $loader)));
        $loader->setParam("MAGISTRAL_PORTS", $this->getMagistralPorts($this->data, $loader));
        $loader->setParam("DHCP_SERVER_IP", $this->data->getDhcpRelay());
        $loader->setParam("VLAN_INET", $this->data->getInetVlan());
        $loader->setParam("PROFILE_5_DENY", $this->getAbonPortsFor5Profile($this->data, $loader));
        $switchParam = $this->data->getSwitchParams();
        $loader->setParam("VLAN_SWITCH", $switchParam['vlanId']);
        $loader->setParam("VLAN_SWITCH_NAME", $switchParam['vlanName']);
        $loader->setParam("SWITCH_IP", $switchParam['ip']);
        $loader->setParam("SWITCH_GETAWAY", $switchParam['gateway']);
        $loader->setParam("SWITCH_CIDR", $switchParam['switchesCidr']);
        $loader->setParam("ACCESS_LOGIN", $switchParam['login']);
        $loader->setParam("ACCESS_PASSWORD", $switchParam['pass']);
        $loader->setParam("ACCESS_COMMUNITY", $switchParam['community']);
        $loader->setParam("PINGER_IP", $this->data->getPingerByIP($switchParam['ip']));
        $loader->setParam("ALL_PORTS_ARR", $this->getAllPortsArr($loader));
        $parser->setLoader($loader);
        return $parser->getCommands();
    }

    function getDbProfiles()
    {
        $addACL = [];
        $binds = $this->data->getBindings();
        $loader = new Loader\StartConfigLoader("MANAGE_PROFILES", $this->data->getDescription());
        foreach ($binds as $bind) {
            $parser = new Loader\ConfigParser();
            $parser->setLoader($loader);
            $loader->setParam("RULE", 2);
            $loader->setParam("IP", $bind['IP']);
            $loader->setParam("PORT", $bind['PORT']);
            $addACL = array_merge($addACL, $parser->getCommands($bind['block']));
        }
        return $addACL;
    }

    protected function getAbonPortsFor5Profile(DataInterface $data, Loader\LoaderInterface $loader)
    {
        $data = $this->getAbonPorts($data, $loader);
        $ret = [];
        foreach ($data as $dat) {
            $ret[] = [
                'PORT' => $dat,
            ];
        }
        return $ret;
    }

    protected function getAbonPorts(DataInterface $data, Loader\LoaderInterface $loader)
    {
        $ab_ports = [];
        $uplinkPort = $data->getMagistralPorts();
        for ($port = 1; $port <= $loader->getDeviceParam()['Ports']['All']; $port++) {
            if (in_array($port, $loader->getDeviceParam()['Ports']['Magistral'])) continue;
            if ($port == $uplinkPort) continue;
            $ab_ports[] = $port;
        }
        return $ab_ports;
    }
    function getAllPortsArr(Loader\LoaderInterface $loader) {
        $all = [];
        for ($port = 1; $port <= $loader->getDeviceParam()['Ports']['All']; $port++) {
            $all[] = ["PORT"=>$port];
        }
        return $all;
    }

    protected function getMagistralPorts(DataInterface $data, Loader\LoaderInterface $loader)
    {
        $magistral = $loader->getDeviceParam()['Ports']['Magistral'];
        $magistral[] = $data->getMagistralPorts();
        return join(",", array_unique($magistral));
    }

    protected function firmwareStringFormat($firmwareString)
    {
        $blocks = explode(".", $firmwareString);
        $response = [];
        foreach ($blocks as $num => $block) {
            $response[] = (int)(preg_replace('/[^0-9]/', '', $block));
        }
        return $response;
    }

    protected function checkFirmware($allowFirmware, $realFirmware)
    {
        $formated = $this->firmwareStringFormat($allowFirmware);
        $real = $this->firmwareStringFormat($realFirmware);
        if (strpos($allowFirmware, ">=") !== false) {
            foreach ($formated as $index => $blockValue) {
                if ($real[$index] >= $blockValue) continue;
                return false;
            }
            return true;
        } elseif (strpos($allowFirmware, "~") !== false) {
            foreach ($formated as $index => $blockValue) {
                if ($real[$index] == $blockValue || ($index + 1) == count($formated)) continue;
                return false;
            }
            return true;
        } elseif (strlen($allowFirmware) != 0) {
            foreach ($formated as $index => $blockValue) {
                if ($real[$index] == $blockValue) continue;
                return false;
            }
            return true;
        }
        throw  new InvalidArgumentException("Empty allow version");
    }
}