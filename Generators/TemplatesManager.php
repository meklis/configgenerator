<?php

namespace Meklis\ConfigGenerator\Generators;


use Meklis\ConfigGenerator\Exceptions\LoadConfigException;

class TemplatesManager
{
    function __construct()
    {
    }

    function getTemplatesList()
    {
        $templatesList = scandir(__DIR__ . "/../Templates");
        if (!$templatesList) throw new LoadConfigException("Error reading templates dir");
        return array_values(array_filter($templatesList, function ($template) {
            $ignoreNames = [".", "..", "README.txt", "README.md"];
            if (in_array($template, $ignoreNames)) return false;
            return true;
        }));
    }
}