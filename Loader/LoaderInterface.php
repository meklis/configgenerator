<?php

namespace Meklis\ConfigGenerator\Loader;


interface LoaderInterface
{
    function getParams();

    function getTemplate();

    function setParam($name, $value);

    function getDeviceParam();

    function getFirmwareRevision();
}